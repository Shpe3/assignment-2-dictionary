from subprocess import Popen
from subprocess import PIPE

input_string = [
        ".", ",", "   ", "1", "2", "=+ght:;""<>*/$%%^&@!GOIUGO", "d3fRtD397", "%$^*#^#&5dgt_-Egvyvwduyva"
        "01100001 01101100 01110010 01100100 01001011 01101001 01101100 01101100 01101101 01100101 ",
        "Right Here", "testestest testestest ", "wut" * 1000, "ng" * 255, "fggd$%%^gd" * 99, "", 
        "6E79617073696C6F6E", "Fg6FGgHJHHS\n\tDFBHDfhyDS", "7fg6ho_=p88", "\t", "\t\n", "\t%%\t \t\t/n\t"
        ]

dictionary = {
        "one": "rgt",
        "two": "fsefs",
        "tri": "3er34rrqraw4fr"
}

def check(input_str):
    return dictionary.get(input_str, "")


def print_test_data(stdin, stdout, stderr):
    print(f"stdin: '{stdin}'")
    print(f"stdout: '{stdout}'")
    print(f"stderr: '{stderr}'")

test_count = len(input_string)

matc_info = False
    
if __name__ == "__main__":
    print("Tests launching...\n")

    command = './program'
    wrong_count = 0
    for input_str in input_string:
        proc = Popen(command, stdin=PIPE, stdout=PIPE, stderr=PIPE, encoding='utf-8')
        stdout, stderr = proc.communicate(input=input_str + "\n")
        right_stdout = check(input_str)
        
        if stdout != right_stdout:
            wrong_count += 1
            print("Wrong answers:")
            print_test_data(input_str, stdout, stderr)
            print(f"expected stdout: '{right_stdout}'")
            print('-' * 20)
        elif matc_info:
            print("Matching return")
            print_test_data(input_str, stdout, stderr)
            print('-' * 20)
        
        print("\nTests finished.")
        if wrong_count == 0:
            print(f"Correct: {test_count}/{test_count}")
        else:
            print(f"Wrong returns: {wrong_count}")
            print(f"Total tests: {test_count}")
            exit(1)
