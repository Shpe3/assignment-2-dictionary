%include "lib.inc"

section .text
global find_string

find_string:
	push r13
	push r14
	mov r13, rdi
	mov r14, rsi

	search_loop:
		mov rax, [r14]
		or rax, rax     
		jz not_found    
		push rdi        
		push rsi          
		mov rdi, r13      
		mov rsi, rax      
		call string_equals
		pop rsi           
		pop rdi         
		test rax, rax    
		jnz found        
		mov r14, rax    
		jmp search_loop
	found:
		mov rax, r14     
		pop r14       
		pop r13   
		ret  
	not_found:
		xor rax, rax      
		pop r14
		pop r13           
		ret            