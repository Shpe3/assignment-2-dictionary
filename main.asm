%include "lib.inc"
%include "colon.inc"
%include "words.inc"
extern find_string
%define BUFFER_SIZE 256
%define BYTE_SIZE 8

section .rodata
    message_not_found db "Element not found", 0
    message_invalid_input db "Invalid input", 0

section .bss
    buffer resb BUFFER_SIZE

section .text
global _start
_start:
    mov edi, buffer
    mov esi, BUFFER_SIZE
    call read_word
    test eax, eax
    jz invalid_input
    mov rdi, buffer
    mov rsi, NEXT_KEY
    call find_string
    test eax, eax
    jz not_found
    lea rdi, [rax + BYTE_SIZE + 1]
    call print_out
    jmp end_program

not_found:
    mov edi, message_not_found
    call print_err
    jmp end_program

invalid_input:
    mov edi, message_invalid_input
    call print_err

end_program:
    call exit
